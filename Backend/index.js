const bcrypt = require('bcrypt')
const SALT_ROUNDS = 10

const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET  

const  mysql = require('mysql')
const connection = mysql.createConnection({
    host : 'localhost',
    user: 'worker_admin',
    password : 'Thapakon', 
    database : 'CheckInCheckOut'
})

connection.connect()
const express = require('express')
const { query, request } = require('express')
const app = express()
const port = 4000

function authenticateToken(req, res, next){
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]
    if(token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err){
            return res.sendStatus(403)
        }
        req.user = user
        next()
    })
}

app.get("/list_worker", (req, res) => {
    query = "SELECT * from Worker";
    connection.query( query, (err, rows) => {
     if (err){
         res.json({
             "status" : "400",
             "message" : "Error quering from running db"
         })
     }else{
         res.json(rows)
     }
    })
})

app.get("/insert_worker", (req,res) => {
    let name = req.query.name
    let surname = req.query.surname
    let job = req.query.job
    let query = `INSERT INTO Worker (WorkerName, WorkerSurname, WorkerJob)
                VALUES ('${name}', '${surname}', '${job}')`

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                    "status" : "400", 
                    "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding worker successful"
            })
        }
    });
})

app.post("/insert_checkIn", (req,res) => {
    let user_id = req.user.user_id
    let query = `INSERT INTO CheckIn (WorkerID)
                VALUES ('${user_id}')`

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                    "status" : "400", 
                    "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding worker successful"
            })
        }
    });
})

app.post("/insert_checkOut", (req,res) => {
    let {user_id} = req.query
    let query = `INSERT INTO CheckOut (WorkerID)
                VALUES ('${user_id}')`

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                    "status" : "400", 
                    "message" : "Error inserting data into db"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Adding worker successful"
            })
        }
    });
})

app.post("/update_worker", (req, res) =>{
    let id = req.query.id
    let name = req.query.name
    let surname = req.query.surname
    let job = req.query.job

    let query =  `UPDATE Worker SET 
                    WorkerName= '${name}',
                    WorkerSurname='${surname}',
                    WorkerJob='${job}'
                    where WorkerID= ${id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                        "status" : "400", 
                        "message" : "Error updating record"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Updating worker successful"
            })
        }
        });
})


app.post("/delete_worker", (req, res) =>{
    let id = req.query.id

    let query =  `DELETE FROM Worker WHERE WorkerID=${id} `

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err){
            console.log(err)
            res.json({
                        "status" : "400", 
                        "message" : "Error deleting record"
                    })
        }else{
            res.json({
                "status" : "200", 
                "message" : "Deleting worker successful"
            })
        }
        });
})

app.get("/list_CheckINCheckOut", (req,res) => {
    let query = `SELECT Worker.WorkerID, Worker.WorkerName, Worker.WorkerSurname, WorkerJob, CheckInID, CheckOutID, CheckIn.TimeForCheckingIn, CheckOut.TimeForCheckingOut
                
                FROM CheckIn, CheckOut, Worker
                WHERE (CheckIn.WorkerID = Worker.WorkerId) AND (CheckIn.CheckInID = CheckOut.CheckOutID)`
    connection.query( query , (err, rows) => {
        if(err){
            res.json({
                "status":"400",
                "message" : "Erroe querying from eunning db"
            })
        }else{
            res.json(rows)
        }
    })
})    

app.post("/register_worker", authenticateToken, (req, res) => {
    let {name, surname, job, username, password} = req.query

    bcrypt.hash(password, SALT_ROUNDS, (err, hash)=>{
        let query =  `INSERT INTO Worker
                (WorkerName, WorkerSurname, WorkerJob, Username, Password, IsAdmin) 
                VALUES ('${name}', '${surname}','${job}',
                        '${username}', '${hash}', false)`
        console.log(query)

        connection.query(query, (err, rows) => {
            if (err){
                console.log(err)
                res.json({
                            "status" : "400", 
                            "message" : "Error inserting data into db"
                        })
            }else{
                res.json({
                    "status" : "200", 
                    "message" : "Adding new worker successful"
                })
            }
        })
    })
})

app.post("/login", (req,res) => {
    let username =  req.query.username
    let user_password =  req.query.password
    let query = `SELECT * FROM Worker WHERE Username='${username}'`
    connection.query( query,(err, rows)=> {
        if (err){
            res.json({
                        "status" : "400", 
                        "message" : "Error querying from running db"
                    })
        }else{
            console.log(rows[0])
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result)=>{
                if(result){
                    let payload = {
                        "username" : rows[0].Username,
                        "user_id" : rows[0].WorkerID,
                        "IsAdmin" : rows[0].IsAdmin,
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, { expiresIn : '1d'})
                    res.json(token)
                }else{
                    res.send("Invalid username / password")
                }
            })
        }
    })
})
 

app.listen(port, ()=>{
    console.log(`Now starting Runinng System Backend at ${port}`)
})
